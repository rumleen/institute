/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package institute;

/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class Student {
    // data variable
    String name;
    int id;

// constructor
public Student(String name, int id) {
     this.name = name;
     this.id = id;
    }
// getter and setter method
public String getName() {
     return name;
    }

public void setName(String name) {
     this.name = name;
    }

public int getId() {
     return id;
    }

public void setId(int id) {
     this.id = id;
    }
}
