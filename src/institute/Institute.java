/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package institute;

/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class Institute {

    // data variable
    String name;
    Department dept;

// constructor
public Institute(String name, Department dept) {
     this.name = name;
     this.dept = dept;
    }

// getter and setter method
public String getName() {
     return name;
    }


public void setName(String name) {
     this.name = name;
    }


public Department getDept() {
     return dept;
    }


public void setDept(Department dept) {
     this.dept = dept;
    }

// user defined method
void display(){  
     System.out.println(name);  
     System.out.println(dept.name);  
     }  
    
}
