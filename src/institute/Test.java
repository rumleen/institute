/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package institute;

/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class Test {
    // main method
public static void main(String[] args) {
 
 // create an object of student class
 Student std = new Student("Roman",101);
 // create an object of Department class
 Department dept = new Department("Jhon", std);
 // create an object of Institute class
 Institute inst = new Institute("Noora", dept);
 
 // call the method
 dept.display();
 inst.display();
    }
}
