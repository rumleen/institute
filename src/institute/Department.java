/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package institute;

/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class Department {
    // data variable
    String name;
    Student std;

// constructor
public Department(String name, Student std) {
     this.name = name;
     this.std = std;
    }

// getter and setter method
public String getName() {
     return name;
    }


public void setName(String name) {
     this.name = name;
    }


public Student getStd() {
     return std;
    }


public void setStd(Student std) {
     this.std = std;
    }

// user defined method
void display(){  
     System.out.println(name);  
     System.out.println(std.name+" "+std.id);  
     }  
}
